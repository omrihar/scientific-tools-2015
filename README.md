TOOLS OF THE COMPUTATIONAL SCIENTIST
====================================

Domains of activity
-------------------

1. Computing Environment
    - Operating System (GNU/Linux, Mac, Windows)
    - Terminal (Guake, Gnome-Terminal, tmux, GNU/Screen)
    - Editing Text
        - **Vim**
            + <http://vimcasts.org>
            + <http://vimawesome.com>
        - Emacs
        - Sublime
            + <https://www.sublimetext.com/>
        - (TextMate)
        - (Gedit)

2. Reading / Organizing Bibliography
    - **Mendeley**
        + <http://mendeley.com>
    - Zotero
        + <http://zotero.org>
    - **Google Scholar**
        + <http://scholar.google.com>
    - Web of science
        + <http://www.webofknowledge.com>
    - Library website
        + <http://uba.uva.nl/en>

3. Writing
    - **Markdown** (Pandoc)
        + <https://daringfireball.net/projects/markdown/>
        + <http://pandoc.org/README.html>
    - **LaTeX** (vim, TexStudio, ...)
        + <http://www.overleaf.com>
        + <http://www.codecogs.com>
    - OpenOffice / Word (+MathType)

4. Programming
    - Version control 
      - **git**, mercurial, ...
          + [bitbucket.org](http://www.bitbucket.org), [github.com](http://github.com), ...
    - **Python**
        - Jupyter (formerly IPython Notebook)
            + <http://jupyter.org>
        - PyCharm, Anaconda, Spyder 
        - Numpy, Scipy, matplotlib, pandas, seaborn, scikit-learn, ...
            + <https://www.youtube.com/playlist?list=PLYx7XA2nY5Gcpabmu61kKcToLz0FapmHu>
    - C / C++ / Fortran / Go / Ruby / Julia / ...

5. Presenting
    - LaTeX
        + <https://en.wikibooks.org/wiki/LaTeX/Presentations>
    - Prezi
    - PowerPoint / Impress

6. Visualizations
    - **Matplotlib**
    - mpld3
    - D3
    - (Excel)
    - ...

7. Mathematics
    - **Python** (Numpy, Scipy, Sympy, StatsModels, Pandas,)
    - SageMath (based on Python)
        + <http://www.sagemath.org>
    - Matlab / Mathematica / Maple
    - R (R-studio)

8. Knowledge storage (Lab journal)
    - Evernote
    - **OneNote**
    - **Bitbucket repository**
    - Workflowy
        + <https://workflowy.com>
        + <https://medium.com/@amirmasoudabdol/workflowy-journal-d33405065d64>
    - Zotero